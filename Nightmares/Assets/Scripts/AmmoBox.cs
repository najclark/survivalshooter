﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MonoBehaviour {

    public GameObject player;
    


    void OnTriggerEnter(Collider other) 
    {
		if(other.tag == "Player")
        {
			PlayerShooting.ammo += 500;
            Destroy(gameObject);
            Debug.Log("ammo hit");

			if (PlayerShooting.ammo > 500) {
				PlayerShooting.ammo = 500;
			}
        }
    }
}
