﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstAidManager : MonoBehaviour
{

    public PlayerHealth playerHealth;
    public GameObject firstaid;
    public float firstaidspawnTime = 3f;
    public Transform[] firstaidspawnPoints;
    Text text;


    void Start()
    {
        InvokeRepeating("FirstAidSpawn", firstaidspawnTime, firstaidspawnTime);
    }



    void FirstAidSpawn() //does not work - "object reference not set to an instance of an object"
    {
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        int firstaidspawnPointIndex = Random.Range(0, firstaidspawnPoints.Length);

        Instantiate(firstaid, firstaidspawnPoints[firstaidspawnPointIndex].position, firstaidspawnPoints[firstaidspawnPointIndex].rotation);
    }
}
