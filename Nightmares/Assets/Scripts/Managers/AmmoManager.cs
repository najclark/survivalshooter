﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoManager : MonoBehaviour
{


    public GameObject ammo;
    public float ammospawnTime = 3f;
    public Transform[] ammospawnPoints;
    Text text;


    void Awake()
    {
        text = GetComponent<Text>();

    }


    void Update()
    {
        text.text = "Ammo: " + PlayerShooting.ammo;
    }
}