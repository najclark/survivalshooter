﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstAidBox : MonoBehaviour {

    public GameObject player;
    PlayerHealth playerHealth;

	void Awake () {
		playerHealth = GetComponent<PlayerHealth> ();
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") //not recognising collision even though it should be the same as the AmmoBox
        {
            playerHealth.currentHealth += 100; //has an issue with this line when it gives me the error
            Destroy(gameObject);
            Debug.Log("first aid hit");

            if (playerHealth.currentHealth > 100)
            {
                playerHealth.currentHealth = 100;
            }
        }
    }
}
